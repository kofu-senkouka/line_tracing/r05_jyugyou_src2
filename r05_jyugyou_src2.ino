volatile uint32_t *GPIO_OUTPORT = (uint32_t *)(0xD0000010);

#define TEST1_LED 12
#define TEST2_LED 13

void setup() {
  // put your setup code here, to run once:
  main_wait_ini(50);//main処理の待ち時間50msにセット
  pinMode(TEST1_LED, OUTPUT);//LED1出力モード
  pinMode(TEST2_LED, OUTPUT);//LED2出力モード

}

void loop() {
  // put your main code here, to run repeatedly:
  unsigned long main_wait_time;//メイン周期の待った時間（us）

  main_wait_time = main_wait();//メイン周期待ち関数
  //前回の設定値が1(点灯)
  if( (*GPIO_OUTPORT & (uint32_t)(1<<12)) == (uint32_t)(1<<12) ){
    *GPIO_OUTPORT = *GPIO_OUTPORT & (uint32_t)~(1<<12);//消灯
  }
  else{
      *GPIO_OUTPORT = *GPIO_OUTPORT | (uint32_t)(1<<12);//点灯
  }

}
